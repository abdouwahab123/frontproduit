import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProduitService } from '../providers/produit.service';

@Component({
  selector: 'app-affich-one',
  templateUrl: './affich-one.component.html',
  styleUrls: ['./affich-one.component.scss']
})
export class AffichOneComponent implements OnInit {

  pizza: any;
  constructor(private route: ActivatedRoute, private produitService: ProduitService) { }

  ngOnInit() {
    const idProduit = this.route.snapshot.params.id;
    this.produitService.get(idProduit).subscribe(
        (data: any) => {
            this.pizza = data.body;
        },
        (error: any) => {
          alert('Erreur lors de la récupération du pizza ');
          console.log(error);
        }
    );
  }

}

 