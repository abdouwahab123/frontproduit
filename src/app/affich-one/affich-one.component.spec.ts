import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffichOneComponent } from './affich-one.component';

describe('AffichOneComponent', () => {
  let component: AffichOneComponent;
  let fixture: ComponentFixture<AffichOneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffichOneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffichOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
