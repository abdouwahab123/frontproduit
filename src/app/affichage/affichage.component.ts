import { Component, OnInit } from '@angular/core';
import { ProduitService } from '../providers/produit.service';

@Component({
  selector: 'app-affichage',
  templateUrl: './affichage.component.html',
  styleUrls: ['./affichage.component.scss']
})
export class AffichageComponent implements OnInit {
  produits: any = [];
  constructor(private produitService: ProduitService) { }

  ngOnInit() {
    this.produitService.liste().subscribe(
        (data: any) => {
          this.produits = data.body;
        }
    );
  }

  delete(id) {
    this.produitService.delete(id).subscribe(
        (success) => {
          this.produits.map((produit, index) => {
            if (produit._id == id) {
              this.produits.splice(index,1);
            }
          });
        }, error => {
          alert('une erreur s\'est produite lors de la suppression');
          console.log(error);
        }
    );
  }

}
