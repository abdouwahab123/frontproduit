import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProduitService } from '../providers/produit.service';

@Component({
  selector: 'app-ajout',
  templateUrl: './ajout.component.html',
  styleUrls: ['./ajout.component.scss']
})
export class AjoutComponent implements OnInit {

  form: FormGroup;
  constructor(private produitService: ProduitService) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = new FormGroup({
      nom: new FormControl('', Validators.compose([
        Validators.required
      ])),
      taille: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      dateExp: new FormControl('')
    });
  }

  create(form) {
    if (!form.valid) {
      alert('Remplissez tous les champs ');
    } else {
      this.produitService.add(form.value).subscribe(
          (rep: any) => {
            alert('Envoi avec succés');
            this.initForm();

          },
          error => {
            alert('erreur lors de l\'enregistrement');
            // console.log('error lors de l\'ajout ',error);
          }
      );
    }
  }

}

