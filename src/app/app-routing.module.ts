import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AffichageComponent } from './affichage/affichage.component';
import { ModifComponent } from './modif/modif.component';
import { AjoutComponent } from './ajout/ajout.component';
import { AffichOneComponent } from './affich-one/affich-one.component';


const routes: Routes = [
  {path: '',  component: AffichageComponent},
  { path: 'produit/new', component: AjoutComponent },
  { path: 'produit/:id/edit', component: ModifComponent },
  { path: 'produit', component: AffichageComponent },
  // { path: 'auth', component: AuthComponent },
  { path: 'produit/:id/show', component: AffichOneComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
