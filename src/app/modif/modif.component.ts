import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProduitService } from '../providers/produit.service';

@Component({
  selector: 'app-modif',
  templateUrl: './modif.component.html',
  styleUrls: ['./modif.component.scss']
})
export class ModifComponent implements OnInit {

  form: FormGroup;
  constructor(private route: ActivatedRoute, private produitService: ProduitService) { }

  ngOnInit() {
    this.produitService.get(this.route.snapshot.params.id).subscribe(
        (produit: any) => {
          this.initForm(produit.body);
        }, error => {

        }
    );

  }

  initForm(produit) {
    this.form = new FormGroup({
      nom: new FormControl(produit.nom, Validators.compose([
        Validators.required
      ])),
      taille: new FormControl(produit.taille, Validators.compose([
        Validators.required,
      ])),
      dateExp: new FormControl('')
    });
  }

  update(form) {
    if (!form.valid) {
      alert('Remplissez tous les champs ');
    } else {
      this.produitService.edit(this.route.snapshot.params.id, form.value).subscribe(
          (rep: any) => {
            alert('Produit modifié avec succés');
            this.initForm(rep.body);

          },
          error => {
            alert('erreur lors de l\'enregistrement');
            console.log('error lors de l\'ajout ', error);
          }
      );
    }
  }

}

