import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AffichageComponent } from './affichage/affichage.component';
import { ModifComponent } from './modif/modif.component';
import { AjoutComponent } from './ajout/ajout.component';
import { AffichOneComponent } from './affich-one/affich-one.component';
import { ProduitService } from './providers/produit.service';

@NgModule({
  declarations: [
    AppComponent,
    AffichageComponent,
    ModifComponent,
    AjoutComponent,
    AffichOneComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule
  ],
  providers: [
    ProduitService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
